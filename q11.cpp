#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
  std::vector<int> v1{ 50,200,800,35,75,150,190,750,900 };
  std::vector<int> v2;

  // operation might use << >> shift for optimization
  std::transform(v1.begin(), v1.end(), std::back_inserter(v2), [](int iValue) -> int{
    if (iValue < 100)
      iValue *= 2;
    if (iValue > 600)
      iValue /= 4;
    return iValue;
    });

  for (int iValue : v2)
    std::cout << iValue << " ";
  return 0;
}
