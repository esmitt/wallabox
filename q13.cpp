#include <iostream>
#include <functional>

// interface for RFID components
class RFID {
public:
	virtual void GetCode() = 0;
	virtual void InstallDriver() = 0;
};

// In the future, functions from here could be put in the RFID (reuse)
class ExistingRFID : RFID {
public:
	ExistingRFID() = default;
	void GetParticularFunc() {
		std::cout << "I'm not your RDIF-father" << std::endl;
	}

	void GetCode() override {
		GetParticularFunc();
	}

	void InstallDriver() override {
		std::cout << "Installing my drivers" << std::endl;
	}
};

// This is the new component to add
class NewRFID : RFID{
public:
	NewRFID() = default;
	void GetNewPainFunctionalities() {
		std::cout << "I'm the new one" << std::endl;
	}

	void GetCode() override {
		GetNewPainFunctionalities();
	}

	void InstallDriver() override {
		std::cout << "Installing my drivers" << std::endl;
	}
};

// Class to handle different RFID devices
class RFIDHandler {
private:
	std::function<void()> m_request;

public:
	RFIDHandler(ExistingRFID* rfid) {
		m_request = [rfid]() {
			rfid->InstallDriver();
			rfid->GetCode(); };
	}
	
	// This could include driver setup as well
	RFIDHandler(NewRFID* rfid) {
		m_request = [rfid]() {
			rfid->InstallDriver();
			rfid->GetCode(); };
	}

	void GetCode() {
		m_request();
	}
};

// Enum that should be change each time a new device is added
enum class RFIDDevices {
	classic,
	newOne
};

// This represents the existing class as well, who handle the state machine
class SuperClass {
private:
	RFIDDevices m_device;							// this is to know the current device
	RFIDHandler* m_handler = nullptr;	// this is the key 

public:
	// Not have to stay on the constructor actually
	SuperClass(RFIDDevices device) : m_device(device) {
		if (device == RFIDDevices::classic)
			m_handler = new RFIDHandler(new ExistingRFID());
	}

	~SuperClass(){
		if (m_handler)
			delete m_handler;
	}

	// here operations to invoke the RFID data/barcode
	void GetMeDataPlease() {
		m_handler->GetCode();
	}
	
	// adding the existing ones
	void ChangeRFID(RFIDDevices device) {
		if (device == RFIDDevices::newOne)
			m_handler = new RFIDHandler(new NewRFID());
		if (device == RFIDDevices::classic)
			m_handler = new RFIDHandler(new ExistingRFID());
	}
};

int main() {
	SuperClass wholeSystem(RFIDDevices::classic);
	wholeSystem.GetMeDataPlease();
	
	std::cout << "it's been 84 years ..." << std::endl;
	std::cout << "..." << std::endl;

	wholeSystem.ChangeRFID(RFIDDevices::newOne);
	wholeSystem.GetMeDataPlease();
}