#include <iostream>
#include <vector>
#include <algorithm>

// max_energy should be a float according to the example or just a template
class ElectricVehicle
{
public:
  ElectricVehicle(std::string p_brand, std::string p_model, int p_energy) : brand(p_brand), model(p_model), max_energy(p_energy) {}

  int getMaxEnergy() { return max_energy; };
private:
  int max_energy;
  std::string model;
  std::string brand;
};

int main()
{
  std::vector<ElectricVehicle> vecVehicles{ElectricVehicle("Tesla", "Model 3", 75), ElectricVehicle("Kia", "Soul", 64), ElectricVehicle("BMW", "i3", 42), ElectricVehicle("Renault", "Zoe", 52), ElectricVehicle("Peougeot", " e-208", 50) };
  std::sort(vecVehicles.begin(), vecVehicles.end(), [](ElectricVehicle& first, ElectricVehicle& second) { return first.getMaxEnergy() < second.getMaxEnergy(); });
  
  for (auto vehicle : vecVehicles)
    std::cout << vehicle.getMaxEnergy() << " ";
  return 0;
}