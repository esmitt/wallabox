#include <iostream>
#include <array>
#include <list>

/// <summary>
///  A class to represent the simple datapackage
/// </summary>
class DataPackage {
public:
	DataPackage() = default;
	DataPackage(int iData) : m_iData(iData) {}
	int GetData() { return m_iData; }
private:
	int m_iData;
};

// interfaces for the "subject" and observer
class IObserver {
public:
	virtual void DataArrivedIt(DataPackage& dataPackage) = 0;
};

class IReceiver {
	virtual void Attach(IObserver* observer) = 0;
	virtual void Detach(IObserver* observer) = 0;
	virtual void Notify() = 0;
};

/// I assumed that data is managed from outside and this object only received it
/// For instance, a Factory to create instances of the data package
class Receiver : public IReceiver {
public:
	void Attach(IObserver* observer) override {
		m_listObserver.push_back(observer);
	}
	void Detach(IObserver* observer) override {
		m_listObserver.remove(observer);
	}
	void Notify() override {
		std::list<IObserver*>::iterator iterator = m_listObserver.begin();
		while (iterator != m_listObserver.end()) {
			(*iterator)->DataArrivedIt(m_dataPackage);
			++iterator;
		}
	}
	void AddData(DataPackage& dataPackage) {
		m_dataPackage = dataPackage;
		Notify();
	}

private:
	std::list<IObserver*> m_listObserver;
	DataPackage m_dataPackage;
};

// Class to simulate the existing one running
// I am assuming that I CAN modify it from beginning (otherwise should be a visitor-based pattern for example)
// #TODO: change the ID assignment (sorry if you're here)
class RunningObject : public IObserver{
private:
	Receiver& _receiver;
	static int m_iID;	// this is a trick to keep the state (not recommended)
	int m_ID;

public:
	// register the object & save the ID
	RunningObject(Receiver& receiver) :_receiver(receiver){
		_receiver.Attach(this);
		m_ID = ++RunningObject::m_iID;
	}
	
	// receiving the data
	void DataArrivedIt(DataPackage& dataPackage) override {
		std::cout << "Data received: "<< dataPackage.GetData() <<" in object with ID: " << m_ID << std::endl;
	}

	// to clean/delete from the subject
	void RemoveThis() {
		_receiver.Detach(this);
	}
};
int RunningObject::m_iID = 0;


// driver code
int main() {
	Receiver receiver;
	DataPackage data1(1);
	DataPackage data2(2);
	DataPackage data3(3);
	
	// register
	RunningObject obj1(receiver);
	RunningObject obj2(receiver);

	// receiving data (this could be usig dynamic approach actually)
	receiver.AddData(data1);
	receiver.AddData(data2);
	receiver.AddData(data3);
	
	obj1.RemoveThis();
	obj2.RemoveThis();
}