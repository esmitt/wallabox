#include <iostream>
#include <tuple>

// O(n)
int F()
{
  int iSum = 0;
  const int LIMIT = 1000;

  for (size_t iIndex = 1; iIndex < LIMIT; iIndex++)
    if (iIndex % 3 == 0 || iIndex % 5 == 0)
      iSum += iIndex;
  return iSum;
}

// O(1), could be a constexpr depending of the compiler
unsigned int F1() 
{
  // 1000 // 3 = 333 and 1000 // 5 = 200 and 1000 // 15 = 66
  const unsigned int sumOfThree = (333 * (1 + 333)) * 3 / 2;
  const unsigned int sumOfFive = (200 * (1 + 200)) * 5 / 2;
  const unsigned int sumOfFifteen = (66 * (1 + 66)) * 15 / 2;

  return sumOfThree + sumOfFive - sumOfFifteen;
}

int main()
{
  std::cout << F() << std::endl;
  std::cout << F1() << std::endl;
  return 0;
}