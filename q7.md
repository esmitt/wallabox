# Data packets are received from 3 different channels. The packets
# must reach two objects that handle several aspects of a process. How
# would you design the class hierarchy and create/initialize the objects
# so it fulfills the requisiste?

The solution depends on some factors:
- The packets indeed achieve objects in order? First to one object, after the other?
- What kind of packages are we talking to? Which is the response time? Is it possible to enqueue (using a future), to be handled by a pool worker (threads)
- Channels are already handled by some mechanism? Channels work in real-time?

If packages have to be in order such as first passed to ObjectA, and after ObjectB, the Command Pattern could be useful to encapsulate packages (execute method) with delivery classes.

In a general way, an approach based on objects communication represents the key. Next, I generate a solution based on the Observer pattern:

A Receiver class which receives the data packages (simulating outside acquisition) implementing the attach/detach/update.
Then, objects RunningObject implement an Observable interface to received data.
Notice that RunningObject keeps an instance of the Receiver to handle the notification when data arrive.

![A visual scheme](q7.png "Class diagram")

[Code](q7.cpp)