#include <iostream>
#include <mutex>

// Simple interface to handle multiple ways
class Output {
public:
	virtual void print() = 0;
};

class OutputAPI : public Output {
public:
	OutputAPI() = default;
	void print() {
		std::cout << ":-D to the server..." << std::endl;
	}
};

class OutputScreen : public Output {
public:
	OutputScreen() = default;
	void print() {
		std::cout << "XD to the screen..." << std::endl;
	}
};

// For now, just the disk as source, but could be added more sources
class Input {
public:
	virtual void read() = 0;
};

class InputDisk : public Input {
public:
	InputDisk() = default;
	void read() override{
		//populate
	}
};

///
/// Singleton to store al variables for configuration, in this example, only the int <var>config</var>
/// Invoking as Config::getInstance().<method>
/// 
class Config {
private:
	int m_config;
	std::mutex m_lock;

public:
	// one way to create the singleton from several ones
	static Config& getInstance() {
		static Config m_instance;
		return m_instance;
	}
	
	// blocking copy constructor and move
	Config(Config const&) = delete;
	Config(Config&&) = delete;
	Config& operator=(Config const&) = delete;
	Config& operator=(Config &&) = delete;

	// critical part to send data to proper output
	void send(Output& output) {
		std::cout << "Prepare data to the output: ";
		const std::lock_guard<std::mutex> lock(m_lock);
		output.print();
	}
	
	// write into disk or to write where is neccesary
	void write() {
		const std::lock_guard<std::mutex> lock(m_lock);
		std::cout << "Writing: " << m_config << std::endl;
	}

protected:
	// read from disk once
	Config() {
		// this is not the best way, just to read directly from disk and that's it
		InputDisk input;
		input.read();  // do nothing, just for demostration purposes
		m_config = 666; 

		std::cout << "read config from disk (" << m_config << ")" << std::endl;
	};
	
	// release resources
	~Config() {
		std::cout << "resources released" << std::endl;
	};
};

// drive code
int main() {
	OutputScreen screen;
	OutputAPI api;
	Config::getInstance().send(screen);
	Config::getInstance().send(api);
	Config::getInstance().write();
	OutputScreen projector;
	Config::getInstance().send(projector);
	Config::getInstance().write();
	return 0;
}