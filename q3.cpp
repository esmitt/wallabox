#include <iostream>
#include <vector>

using int64 = unsigned long int;
using vecpair2 = std::vector<std::pair<int64, int64>>;

vecpair2 OrderedPairs(int64 O, int64 P, int64 A, int64 B)
{
  vecpair2 vecSum;
  for (int64 iIndexO = 1; iIndexO <= O; iIndexO++)
    for (int64 iIndexP = 1; iIndexP <= P; iIndexP++)
    {
      const int iSum = iIndexO + iIndexP;
      if (iSum % A == 0 && iSum % B == 0)
        vecSum.push_back(std::make_pair(iIndexO, iIndexP));
    }
  return vecSum;
}

int main()
{
  vecpair2 vecPairs = OrderedPairs(100, 100, 20, 5);
  for (auto it = vecPairs.begin(); it != vecPairs.end(); ++it)
    std::cout << it->first << " and " << it->second << std::endl;
  return 0;
}
