#include <filesystem>
#include <iostream>
#include <ctime>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <locale>
#include <list>
#include <ctime>

namespace fs = std::filesystem;
using int64 = long long int;

const unsigned int LIMIT_SIZE_BYTES = (1 << 20) * 14;
const std::string MONTHS[13]{ "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

/// Build a date given unchecked dates
std::tm makeDate(int iDay, int iMonth, int iYear)
{
	struct std::tm tmTime;
	tmTime.tm_mday = iDay;
	tmTime.tm_mon = iMonth;
	tmTime.tm_year = iYear;
	return tmTime;
}

/// Print in unix-based permission format string
// from https://en.cppreference.com/w/cpp/filesystem/permissions
std::string PrintPerms(fs::perms p)
{
	std::ostringstream stream;
	stream << ((p & fs::perms::owner_read) != fs::perms::none ? "r" : "-")
		<< ((p & fs::perms::owner_write) != fs::perms::none ? "w" : "-")
		<< ((p & fs::perms::owner_exec) != fs::perms::none ? "x" : "-")
		<< ((p & fs::perms::group_read) != fs::perms::none ? "r" : "-")
		<< ((p & fs::perms::group_write) != fs::perms::none ? "w" : "-")
		<< ((p & fs::perms::group_exec) != fs::perms::none ? "x" : "-")
		<< ((p & fs::perms::others_read) != fs::perms::none ? "r" : "-")
		<< ((p & fs::perms::others_write) != fs::perms::none ? "w" : "-")
		<< ((p & fs::perms::others_exec) != fs::perms::none ? "x" : "-");
	return stream.str();
}

// overloading of << and < operator
class File {
public:
	File(const std::string& strOwner, const fs::perms& perm, const std::tm& dateTime, const int64& byteSize, const std::string filename) {
		m_strOwner = strOwner;
		m_permissions = perm;
		m_dateTime = dateTime;
		m_i64BytesSize = byteSize;
		m_strFilename = filename;
	}

	friend std::ostream& operator<<(std::ostream& os, const File& file);
	
	// comparison of dates
	bool operator <(const File& file) const
	{
		if (this->m_dateTime.tm_year < file.m_dateTime.tm_year)
			return true;

		if (this->m_dateTime.tm_year > file.m_dateTime.tm_year)
			return false;

		if (this->m_dateTime.tm_year == file.m_dateTime.tm_year) {
			if (this->m_dateTime.tm_mon < file.m_dateTime.tm_mon)
				return true;
			else if (this->m_dateTime.tm_mon > file.m_dateTime.tm_mon)
				return false;
			else if (this->m_dateTime.tm_mday < file.m_dateTime.tm_mday)
				return true;
			else if (this->m_dateTime.tm_mday > file.m_dateTime.tm_mday)
				return false;
		}
		return true;  //are equals
	}

	// this should be private using getters or a function accomplish requested conditions
	std::string	m_strOwner;
	fs::perms m_permissions;
	std::tm m_dateTime;
	int64 m_i64BytesSize;
	std::string m_strFilename;
};

std::ostream& operator<<(std::ostream& os, const File& file)
{
	os << file.m_strOwner << " ";
	os << PrintPerms(file.m_permissions) << " ";
	os << file.m_dateTime.tm_mday << " " << MONTHS[file.m_dateTime.tm_mon] << " " << file.m_dateTime.tm_year << " ";
	os << file.m_i64BytesSize << " ";
	os << file.m_strFilename << std::endl;
	return os;
}


// now, the required function
std::string EarliestModifiedFile(const std::list<File>& listFiles) {
	const std::string strOuput = "NO FILES";
	const fs::perms mask = fs::perms::owner_exec | fs::perms::group_exec | fs::perms::others_exec;
	
	std::vector<File> vecSolution;
	for (auto entry : listFiles)
	{
		
		if ((entry.m_i64BytesSize < LIMIT_SIZE_BYTES) && (entry.m_strOwner.compare("admin") == 0) && static_cast<bool>(entry.m_permissions & mask))
			vecSolution.push_back(entry);
	}

	// extract the 1st position of the sorted vector
	if (vecSolution.size() > 0) {
		std::sort(vecSolution.begin(), vecSolution.end());
		return vecSolution[0].m_strFilename;
	}
	return strOuput;
}

int main()
{
	std::list<File> listFiles{File("admin", fs::perms::owner_write | fs::perms::owner_exec, makeDate(29, 9, 1983), 833, "source.h"),
														File("admin", fs::perms::owner_read | fs::perms::group_exec, makeDate(23, 1, 2003), 854016, "blockbuster.mpeg"),
														File("admin", fs::perms::others_exec, makeDate(2, 7, 1997), 821, "delete-this.py"),
														File("admin", fs::perms::group_write | fs::perms::owner_exec, makeDate(15, 2, 1971), 23552, "library.dll"),
														File("admin", fs::perms::group_all, makeDate(15, 5, 1979), 645922816, "logs.zip"),
														File("jane", fs::perms::others_read, makeDate(4, 12, 2010), 93184, "old-photos.zip"),
														File("jane", fs::perms::group_read, makeDate(8, 2, 1982), 681574400, "important.java"),
														File("admin", fs::perms::all, makeDate(26, 12, 1952), 14680064, "to-do-list.txt") };
	
	// just to see values on list
	for (auto entry : listFiles)
		std::cout << entry;
		

	std::cout << EarliestModifiedFile(listFiles) << std::endl;
}