#include <iostream>

struct Vehicle {
	virtual ~Vehicle() = default;
	virtual void Move() {
		std::cout << "Vehicle::Move" << std::endl;
	}
};

// avoiding the diamond effect
struct WheelVehicle : virtual Vehicle {
	virtual void TurnRight() {}
};

struct WingVehicle : virtual Vehicle {
	virtual void TakeOff() {}
};

struct Plane : WheelVehicle, WingVehicle {};

Plane plane;

int main() {
	Plane p;
	p.Move();
	Vehicle& v = p;
	v.Move();
	return 0;
}

/*
Answer to questions:
why? 
ambiguity for the multiple inheritance (diamond)

How would you call the Move() method from a plane?
Just invoking it using the virtual function from Vehicle

How would you solve this problem?
Adding the inherance as virtual
*/