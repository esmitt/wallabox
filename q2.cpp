#include <iostream>
// C header
#include <stdlib.h>
#include <memory.h>

// definition + print function
struct ev_t {
	int iSomething;
	char arrSomething[3];
	int iOther;
};

void PrintStruct(const ev_t& struct_ev) {
	std::cout << struct_ev.iSomething << std::endl;
	std::cout << struct_ev.arrSomething << std::endl;
	std::cout << struct_ev.iOther << std::endl;
}

int main() {
	ev_t* car = (ev_t*)malloc(sizeof(ev_t));
	if (car != NULL)
	{
		car->iSomething = 9;
		car->arrSomething[0] = 'y';
		car->arrSomething[1] = 'e';
		car->arrSomething[2] = '\0';
		car->iOther = -666;
		std::cout << "The content is: " << std::endl;
		PrintStruct(*car);

		// wrong solution
		std::cout << "Wrong: " << std::endl;
		memset(car, 0, sizeof car);
		PrintStruct(*car);

		std::cout << "one possible solution: " << std::endl;
		memset(&car->iSomething, 0, sizeof(int));
		memset(&car->arrSomething, 0, sizeof(char) * 3); // arrSomething[2] should be \0 instead 0
		memset(&car->iOther, 0, sizeof(int));
		PrintStruct(*car);

	}
	return 0;
}

/*
2. how would you correct this code? What�s wrong with it?
making a memset by each object inside ev_t
Memset just set to _something_ (zero in this case) as chunks of memory
*/