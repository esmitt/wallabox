#include <iostream>
#include <vector>

unsigned int MinimumNumber(const std::vector<bool>& vecCoins) {
	unsigned int iCount = 0;
	for (int iIndex = 0; iIndex < vecCoins.size(); ++iIndex) {

		if ((iIndex&1)==1 && !vecCoins[iIndex])
			iCount++;

		if ((iIndex&1)==0 && vecCoins[iIndex])
			iCount++;
	}
	// count the number of changes or how many stay them
	return std::min<unsigned int>(iCount, vecCoins.size() - iCount);
}

int main() {
	const std::vector<bool> vecCoins{ 1, 0, 1, 0, 1, 0, 0, 1, 1, 0 };
	std::cout << "# of changes: "<< MinimumNumber(vecCoins) << std::endl;
}