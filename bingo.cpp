/*
Develop a program to play bingo:
We should play with 90 balls and cards of 15 positions.
When you execute the program it should:
Generate a card of 15 numbers. All different (non-repeating) numbers, as balls, go from 1 to 90.
Print the card in 3 lines of 5 numbers. This has to be sorted. You can sort the card data structure or print it sorted.
Generate 33 balls of numbers from 1 to 90. Also non-repeating numbers.
Display the balls. The balls do not have to be sorted.
Calculate the hits: Somehow store which numbers from the ball are the same that on the card.
Print the marked card, substituting the �hit� numbers with character X.
Example of output:
Card:
6 12 16 27 34
35 38 42 51 57
68 71 78 84 90
Balls:
55 19 36 13 38 59 54 3 2 79 39 58 78 51 31 49 63 7 53 62 75 26 48 81 67 74 64 1 45 40 41 9 42
Marked card:
6 12 16 27 34
35 X X X 57
68 71 X 84 90
*/
#include <array>
#include <random>
#include <map>
#include <assert.h>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <chrono>
#include <iomanip>

using vInt = std::vector<unsigned char>;

// common interfaces
class IGame {
	virtual void Print(const std::string& strTitle) = 0;
	virtual const vInt GetNumbers() = 0;
};


class Bingo : public IGame {
private:
	vInt m_vecBingo;
	static constexpr int m_iLimit = 33;

public:
	Bingo() = default;
	Bingo(std::default_random_engine generator) {
		std::uniform_int_distribution<int> distribution(1, 90);
		std::map<int, bool> mapUnique;
		int iNumber;
		
		int iRemaining = m_iLimit - mapUnique.size();
		
		while (iRemaining > 0) {
			iNumber = distribution(generator);
			mapUnique[iNumber] = true;
			iRemaining = m_iLimit - mapUnique.size();
		}

		// copy to vector
		for (auto item : mapUnique)
			m_vecBingo.push_back(item.first);
	}

	Bingo(const Bingo& bingo) {
		m_vecBingo.assign(bingo.m_vecBingo.begin(), bingo.m_vecBingo.end());
	}

	// overriding methods
	const vInt GetNumbers() override {
		return m_vecBingo;
	}

	void Print(const std::string& strTitle) override {
		std::cout << strTitle << std::endl;
		std::for_each(m_vecBingo.begin(), m_vecBingo.end(), [](int iValue) {
			std::cout << iValue << " ";
			});
		std::cout << std::endl;
	}
};

class Card : public IGame {
private:
	std::map<int, bool> m_mapCard;

public:
	Card(std::default_random_engine& generator) {
		std::uniform_int_distribution<int> distribution(1, 90);
		const int iLimit = 15;
		int iNumber;
		while (m_mapCard.size() < iLimit) {
			iNumber = distribution(generator);
			m_mapCard[iNumber] = false;
		}	
	}

	void Mark(Bingo bingo) {
		vInt vecNum = bingo.GetNumbers();

		int iNumber;
		for (size_t iIndex = 0; iIndex < vecNum.size(); ++iIndex) {
			iNumber = vecNum[iIndex];
			if (m_mapCard.find(iNumber) != m_mapCard.end())
				m_mapCard[iNumber] = true;
		}
	}

	// overriding methods
	const vInt GetNumbers() override {
		vInt vecNumbers;
		for (auto const& imap : m_mapCard)
			vecNumbers.push_back(imap.first);
		return vecNumbers;
	}

	void Print(const std::string& strTitle) override{
		std::cout << strTitle << std::endl;
		int iIndex = 0;
		for (const auto& item : m_mapCard) {
			std::cout << std::setfill(' ') << std::setw(2);
			if (item.second)
				std::cout << "X ";
			else
				std::cout << item.first << " ";

			if ((iIndex + 1) % 5 == 0)
				std::cout << std::endl;

			iIndex++;
		}
	}

};

// testing solution
template <class T>
void CheckUnique(T vecTest) {
	auto it = std::unique(vecTest.begin(), vecTest.end(), [](auto i, auto j) {
		return (i == j);
		});
	assert(std::distance(it, vecTest.end()) == 0);
}

int main() {
	std::default_random_engine generator;
	auto seed = static_cast<long unsigned int>(std::chrono::high_resolution_clock::now().time_since_epoch().count());
	generator.seed(seed);

	Card card(generator);
	Bingo bingo(generator);

	// test
	CheckUnique(bingo.GetNumbers());
	CheckUnique(card.GetNumbers());
	
	//program
	card.Print("Card:");
	bingo.Print("Balls: ");
	card.Mark(bingo);
	card.Print("Marked card :");
	return 0;
}